# SWG Vision of Hope DSRC
#### A big thanks goes to the Stella Bellum team for open sourcing their repo, and allowing us to use it as our base code.

You can find the sys.shared data [here](https://bitbucket.org/swgvisionofhope/data).

Adding new game objects that were never on live would be added to this repo's sys.shared.