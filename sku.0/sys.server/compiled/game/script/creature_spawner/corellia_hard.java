package script.creature_spawner;

public class corellia_hard extends script.creature_spawner.base_newbie_creature_spawner
{
    public byte maxPop = 3;

    public String pickCreature()
    {
        switch (rand(1, 3))
        {
            case 1:
                return "crazed_durni";
            case 2:
                return "gurrcat";
            default:
                return "sand_panther_cub";
        }
    }
}
