package script.creature_spawner;

public class naboo_easy extends script.creature_spawner.base_newbie_creature_spawner
{
    public byte maxPop = 6;

    public String pickCreature()
    {
        switch (rand(1, 3))
        {
            case 1:
                return "flesh_eating_chuba";
            case 2:
                return "capper_spineflap";
            default:
                return "diseased_nuna";
        }
    }
}
