package script.creature_spawner;

import java.util.ArrayList;
import script.dictionary;
import script.library.*;
import script.location;
import script.obj_id;

import java.util.List;

public class base_newbie_npc_spawner extends script.base_script
{
	public byte maxPop = 3;

	public int OnInitialize(obj_id self)
	{
		spawnCreatures(self);
		return SCRIPT_CONTINUE;
	}
	public void spawnCreatures(obj_id self)
	{
		String whatToSpawn = utils.getStringScriptVar(self, "creatureToSpawn");
		if (whatToSpawn == null)
		{
			whatToSpawn = pickCreature();
			utils.setScriptVar(self, "creatureToSpawn", whatToSpawn);
		}
		if (hasObjVar(self, "pop"))
		{
			maxPop = (byte)getIntObjVar(self, "pop");
			if (maxPop > 20)
			{
				CustomerServiceLog("SPAWNER_OVERLOAD", "MaxPop of Spawner " + self + " was greater than 20.  It was " + maxPop + " in fact.");
			}
		}
		int count = utils.getIntScriptVar(self, "count");
		location goodLoc;
		obj_id spawned;
		List theList;
		while (count < maxPop)
		{
			goodLoc = pickLocation();
			if (goodLoc == null)
			{
				goodLoc = getLocation(self);
			}
			spawned = create.object(whatToSpawn, goodLoc);
			attachScript(spawned, "creature_spawner.death_msg");
			setObjVar(spawned, "creater", self);
			if (!utils.hasScriptVar(self, "myCreations"))
			{
				List<obj_id> myCreations = new ArrayList<>();
				myCreations.add(spawned);
				utils.setScriptVar(self, "myCreations", myCreations);
			}
			else
			{
				theList = utils.getResizeableObjIdArrayScriptVar(self, "myCreations");
				if (theList.size() >= maxPop)
				{
					CustomerServiceLog("SPAWNER_OVERLOAD", "Tried to spawn something even though the list was full.");
					return;
				}
				else
				{
					theList.add(spawned);
					utils.setScriptVar(self, "myCreations", theList);
				}
			}
			if (hasObjVar(self, "useCityWanderScript"))
			{
				attachScript(spawned, "city.city_wander");
			}
			else
			{
				ai_lib.setDefaultCalmBehavior(spawned, ai_lib.BEHAVIOR_LOITER);
			}
			count++;
			utils.setScriptVar(self, "count", count);
		}
	}
	public String pickCreature()
	{
		// note: this method should be overridden in all child classes.  probably should
		// make an interface to enforce this.
		return "flail_cutthroat";
	}
	public location pickLocation()
	{
		location here = getLocation(getSelf());
		here.setX(here.getX() + rand(-5, 5));
		here.setZ(here.getZ() + rand(-5, 5));
		return locations.getGoodLocationAroundLocation(here, 1f, 1f, 1.5f, 1.5f);
	}
	public int creatureDied(obj_id self, dictionary params)
	{
		spawning.planetSpawnersCreatureDied(self, params.getObjId("deadGuy"));
		spawnCreatures(self);
		return SCRIPT_CONTINUE;
	}
}
