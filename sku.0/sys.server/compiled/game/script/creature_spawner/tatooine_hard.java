package script.creature_spawner;

public class tatooine_hard extends script.creature_spawner.base_newbie_creature_spawner
{
    public byte maxPop = 3;

    public String pickCreature()
    {
        switch (rand(1,3))
        {
            case 1:
                return "mound_mite";
            case 2:
                return "desert_squill";
            default:
                return "womprat";
        }
    }
}
