package script.creature_spawner;

public class tatooine_medium extends script.creature_spawner.base_newbie_creature_spawner
{
    public byte maxPop = 4;

    public tatooine_medium()
    {
    }
    public String pickCreature()
    {
        switch (rand(1,3))
        {
            case 1:
                return "rockmite";
            case 2:
                return "mound_mite";
            default:
                return "worrt";
        }
    }
}
