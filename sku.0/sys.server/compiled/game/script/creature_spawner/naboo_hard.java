package script.creature_spawner;

public class naboo_hard extends script.creature_spawner.base_newbie_creature_spawner
{
    public String pickCreature()
    {
        switch (rand(1, 3))
        {
            case 1:
                return "nuna";
            case 2:
                return "rabid_shaupaut";
            default:
                return "narglatch_cub";
        }
    }
}
