package script.event;

import script.dictionary;
import script.library.holiday;
import script.library.utils;
import script.obj_id;

public class holiday_controller extends script.base_script
{
    public int OnAttach(obj_id self)
    {
        if (holiday.ACTIVE_HOLIDAY != 0) {
            startUniverseWideEvent(holiday.ACTIVE_HOLIDAY);
        }
        return SCRIPT_CONTINUE;
    }
    public int OnInitialize(obj_id self)
    {
        if (holiday.ACTIVE_HOLIDAY != 0) {
            startUniverseWideEvent(holiday.ACTIVE_HOLIDAY);
        }
        return SCRIPT_CONTINUE;
    }
    public int OnGetAttributes(obj_id self, obj_id player, String[] names, String[] attribs) {
        int idx = utils.getValidAttributeIndex(names);
        if (idx == -1 || !isGod(player)) {
            return SCRIPT_CONTINUE;
        }
        if (holiday.ACTIVE_HOLIDAY == 1) {
            names[idx] = "holiday_event";
            attribs[idx] = "Halloween Event Running = True";
            idx++;
        } else {
            names[idx] = "holiday_event";
            attribs[idx] = "Halloween Event Running = False";
            idx++;
        }
        if (holiday.ACTIVE_HOLIDAY == 2) {
            names[idx] = "holiday_event";
            attribs[idx] = "Life Day Event Running = True";
            idx++;
        } else {
            names[idx] = "holiday_event";
            attribs[idx] = "Life Day Event Running = False";
            idx++;
        }
        if (holiday.ACTIVE_HOLIDAY == 3) {
            names[idx] = "holiday_event";
            attribs[idx] = "Love Day Event Running = True";
            idx++;
        } else {
            names[idx] = "holiday_event";
            attribs[idx] = "Love Day Event Running = False";
            idx++;
        }
        if (holiday.ACTIVE_HOLIDAY == 4) {
            names[idx] = "holiday_event";
            attribs[idx] = "Empire Day Event Running = True";
        } else {
            names[idx] = "holiday_event";
            attribs[idx] = "Empire Day Event Running = False";
        }
        return SCRIPT_CONTINUE;
    }
    public int OnHearSpeech(obj_id self, obj_id speaker, String text) {
        if (!isGod(speaker)) {
            return SCRIPT_CONTINUE;
        }

        String universeWideEvents = getCurrentUniverseWideEvents();
        int halloween = universeWideEvents.indexOf("halloween");
        int lifeday = universeWideEvents.indexOf("lifeday");
        int loveday = universeWideEvents.indexOf("loveday");
        int empireday = universeWideEvents.indexOf("empireday_ceremony");

        switch (text) {
            case "halloweenStart":
                startHolidayEvent(speaker, 1, halloween);
                return SCRIPT_OVERRIDE;
            case "halloweenStop":
                stopHolidayEvent(speaker, 1, halloween);
                return SCRIPT_OVERRIDE;
            case "halloweenStartForReals":
                startHolidayEventForReals(speaker, 1);
                return SCRIPT_OVERRIDE;
            case "halloweenStopForReals":
                stopHolidayEventForReals(speaker, 1);
                return SCRIPT_OVERRIDE;
            case "lifedayStart":
                startHolidayEvent(speaker, 2, lifeday);
                return SCRIPT_OVERRIDE;
            case "lifedayStop":
                stopHolidayEvent(speaker, 2, lifeday);
                return SCRIPT_OVERRIDE;
            case "lifedayStartForReals":
                startHolidayEventForReals(speaker, 2);
                return SCRIPT_OVERRIDE;
            case "lifedayStopForReals":
                stopHolidayEventForReals(speaker, 2);
                return SCRIPT_OVERRIDE;
            case "lovedayStart":
                startHolidayEvent(speaker, 3, loveday);
                return SCRIPT_OVERRIDE;
            case "lovedayStop":
                stopHolidayEvent(speaker, 3, loveday);
                return SCRIPT_OVERRIDE;
            case "lovedayStartForReals":
                startHolidayEventForReals(speaker, 3);
                return SCRIPT_OVERRIDE;
            case "lovedayStopForReals":
                stopHolidayEventForReals(speaker, 3);
                return SCRIPT_OVERRIDE;
            case "empiredayStart":
                startHolidayEvent(speaker, 4, empireday);
                return SCRIPT_OVERRIDE;
            case "empiredayStop":
                stopHolidayEvent(speaker, 4, empireday);
                return SCRIPT_OVERRIDE;
            case "empiredayStartForReals":
                startHolidayEventForReals(speaker, 4);
                return SCRIPT_OVERRIDE;
            case "empiredayStopForReals":
                stopHolidayEventForReals(speaker, 4);
                return SCRIPT_OVERRIDE;
        }
        return SCRIPT_CONTINUE;
    }
    private void startHolidayEvent(obj_id speaker, int holidayName, int holidayStatus)
    {
        if (holidayName != holiday.ACTIVE_HOLIDAY) {
            sendSystemMessageTestingOnly(speaker, "Server config is not marked as " + holidayName + " running. Instead it is: " + holiday.ACTIVE_HOLIDAY);
            return;
        }
        if (holidayStatus > -1) {
            sendSystemMessageTestingOnly(speaker, "Server says that " + holidayName + " is already running. If you are sure that it's not, say " + holidayName + "StartForReals");
        } else if (holidayStatus < 0) {
            sendSystemMessageTestingOnly(speaker, "Holiday: " + holidayName + " started.");
            startUniverseWideEvent(holidayName);
        }
    }
    private void startHolidayEventForReals(obj_id speaker, int holidayName)
    {
        if (holidayName == holiday.ACTIVE_HOLIDAY)
        {
            sendSystemMessageTestingOnly(speaker, holidayName + " started.");
            startUniverseWideEvent(holidayName);
        }
    }
    private void stopHolidayEvent(obj_id speaker, int holidayName, int holidayStatus)
    {
        if (holidayName == holiday.ACTIVE_HOLIDAY)
        {
            sendSystemMessageTestingOnly(speaker, "Server config is marked as " + holidayName + " running. If you are sure that it should not be running anyway, say " + holidayName + "StopForReals");
        }
    }
    private void stopHolidayEventForReals(obj_id speaker, int holidayName)
    {
        sendSystemMessageTestingOnly(speaker, "Holiday: " + holidayName + " stopped.");
        stopUniverseWideEvent(holidayName);
    }
}
