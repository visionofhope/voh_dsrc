package script.event.gcwlaunch;

import script.library.create;
import script.library.locations;
import script.location;
import script.obj_id;

public class gcwlaunch_spawner extends script.base_script
{
    public static final location[] LOCS = 
    {
        new location(110, 52, -5342),
        new location(-150, 28, -4720),
        new location(4812, 4, -4705),
        new location(-1280, 0, -3590),
        new location(6902, 330, -5550),
        new location(-4897, 6, 4106)
    };
    public int OnInitialize(obj_id self)
    {
        if (!hasObjVar(self, "event.gcwlaunch.spawner"))
        {
            int locStart;
            switch (locations.getGuardSpawnerRegionName(getLocation(self))) {
                case "@tatooine_region_names:anchorhead":
                    locStart = 0;
                    break;
                case "@corellia_region_names:coronet":
                    locStart = 3;
                    break;
                case "@naboo_region_names:moenia":
                    locStart = 6;
                    break;
                case "@tatooine_region_names:bestine":
                    locStart = 9;
                    break;
                case "@corellia_region_names:bela_vistal":
                    locStart = 12;
                    break;
                case "@naboo_region_names:theed":
                    locStart = 15;
                    break;
                default:
                    return SCRIPT_CONTINUE;
            }
            location tmp = LOCS[locStart];
            tmp.setArea(getCurrentSceneName());
            obj_id spawner = create.object("object/tangible/poi/base/poi_base.iff", tmp);
            setObjVar(self, "event.gcwlaunch.spawner", spawner);
            attachScript(spawner, "event.gcwlaunch.gcwlaunch");
        }
        return SCRIPT_CONTINUE;
    }
}
