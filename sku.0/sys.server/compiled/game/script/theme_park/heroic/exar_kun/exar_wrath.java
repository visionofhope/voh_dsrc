package script.theme_park.heroic.exar_kun;

import script.*;

import java.util.ArrayList;
import java.util.List;

import script.library.buff;
import script.library.trial;

public class exar_wrath extends script.base_script
{
    public exar_wrath()
    {
    }
    public int OnAttach(obj_id self)
    {
        trial.setHp(self, 545020);
        setObjVar(self, "isImmobile", true);
        return SCRIPT_CONTINUE;
    }
    public int OnEnteredCombat(obj_id self)
    {
        setMovementPercent(self, 0.0f);
        playClientEffectObj(getHateTarget(self), "appearance/pt_force_meditate.prt", self, "");
        messageTo(self, "executeBlast", trial.getSessionDict(self, "blast"), 4.0f, false);
        messageTo(self, "applyWard", trial.getSessionDict(self, "ward"), 0.0f, false);
        buff.applyBuff(self, "mind_trick_immune");
        return SCRIPT_CONTINUE;
    }
    public int OnExitedCombat(obj_id self)
    {
        trial.bumpSession(self, "blast");
        trial.bumpSession(self, "ward");
        return SCRIPT_CONTINUE;
    }
    public int executeBlast(obj_id self, dictionary params)
    {
        if (!trial.verifySession(self, params, "blast"))
        {
            return SCRIPT_CONTINUE;
        }
        if (isDead(self))
        {
            return SCRIPT_CONTINUE;
        }
        obj_id hateTarget = getHateTarget(self);
        obj_id weapon = getCurrentWeapon(hateTarget);
        int elementalType = getWeaponElementalType(weapon);
        switch (elementalType)
        {
            case DAMAGE_ELEMENTAL_HEAT:
            queueCommand(self, (1097819132), hateTarget, "", COMMAND_PRIORITY_DEFAULT);
            break;
            case DAMAGE_ELEMENTAL_COLD:
            queueCommand(self, (-2065560327), hateTarget, "", COMMAND_PRIORITY_DEFAULT);
            break;
            case DAMAGE_ELEMENTAL_ACID:
            queueCommand(self, (-1503522191), hateTarget, "", COMMAND_PRIORITY_DEFAULT);
            break;
            case DAMAGE_ELEMENTAL_ELECTRICAL:
            queueCommand(self, (-692498043), hateTarget, "", COMMAND_PRIORITY_DEFAULT);
            break;
            default:
            String[] randomBlast = 
            {
                "heat",
                "cold",
                "acid",
                "electrical"
            };
            String blastChoice = randomBlast[rand(0, 3)];
            String blastCommand = "kun_wrath_" + blastChoice;
            queueCommand(self, getStringCrc(blastCommand.toLowerCase()), hateTarget, "", COMMAND_PRIORITY_DEFAULT);
            break;
        }
        messageTo(self, "executeBlast", trial.getSessionDict(self, "blast"), 4.0f, false);
        return SCRIPT_CONTINUE;
    }
    public int applyWard(obj_id self, dictionary params)
    {
        if (!trial.verifySession(self, params, "ward"))
        {
            return SCRIPT_CONTINUE;
        }
        if (isDead(self))
        {
            return SCRIPT_CONTINUE;
        }
        obj_id[] objects = trial.getAllObjectsInDungeon(trial.getTop(self));
        List<String> wards = new ArrayList<>();
        wards.add("kun_wrath_ward_acid");
        wards.add("kun_wrath_ward_heat");
        wards.add("kun_wrath_ward_cold");
        wards.add("kun_wrath_ward_electrical");
        if (buff.hasBuff(self, "kun_wrath_ward_acid"))
        {
            wards.remove("kun_wrath_ward_acid");
        }
        if (buff.hasBuff(self, "kun_wrath_ward_heat"))
        {
            wards.remove("kun_wrath_ward_heat");
        }
        if (buff.hasBuff(self, "kun_wrath_ward_cold"))
        {
            wards.remove("kun_wrath_ward_cold");
        }
        if (buff.hasBuff(self, "kun_wrath_ward_electrical"))
        {
            wards.remove("kun_wrath_ward_electrical");
        }
        String newWard = wards.get(rand(0, wards.size() - 1));
        for (obj_id object : objects)
        {
            if (isPlayer(object) || isMob(object))
            {
                buff.applyBuff(object, newWard);
            }
        }
        messageTo(self, "applyWard", trial.getSessionDict(self, "ward"), 8.0f, false);
        return SCRIPT_CONTINUE;
    }
}
