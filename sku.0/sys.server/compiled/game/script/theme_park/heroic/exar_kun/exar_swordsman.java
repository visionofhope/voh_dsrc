package script.theme_park.heroic.exar_kun;

import script.*;

import script.library.trial;

public class exar_swordsman extends script.base_script
{
    public int OnAttach(obj_id self)
    {
        setCreatureCoverVisibility(self, false);
        setInvulnerable(self, true);
        aiEquipPrimaryWeapon(self);
        return SCRIPT_CONTINUE;
    }
    public int kill_command(obj_id self, dictionary params)
    {
        trial.setHp(self, 415245);
        obj_id[] players = trial.getValidTargetsInCell(trial.getTop(self), "r7");
        obj_id closest = trial.getClosest(self, players);
        startCombat(self, closest);
        return SCRIPT_CONTINUE;
    }
}
