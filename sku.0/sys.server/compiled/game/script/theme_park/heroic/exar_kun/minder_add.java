package script.theme_park.heroic.exar_kun;

import script.*;

import script.library.buff;
import script.library.trial;

public class minder_add extends script.base_script
{
    public int OnAttach(obj_id self)
    {
        buff.applyBuff(self, "minder_add_debuff");
        obj_id[] targets = trial.getValidTargetsInCell(trial.getTop(self), "r3");
        if (targets == null || targets.length == 0)
        {
            trial.cleanupObject(self);
        }
        startCombat(self, targets[rand(0, targets.length - 1)]);
        return SCRIPT_CONTINUE;
    }
}
