package script.theme_park.heroic.exar_kun;

import script.*;

import script.library.buff;
import script.library.trial;

public class exar_vengence extends script.base_script
{
    public int OnAttach(obj_id self)
    {
        trial.setHp(self, 545020);
        return SCRIPT_CONTINUE;
    }
    public int OnEnteredCombat(obj_id self)
    {
        buff.applyBuff(self, "mind_trick_immune");
        return SCRIPT_CONTINUE;
    }
}
