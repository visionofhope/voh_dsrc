package script.working.iosnowore;

import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.ArrayList;
import java.util.List;
import script.base_script;

import script.library.utils;

public class memtest extends script.base_script
{
    public memtest()
    {
    }
    public int OnHearSpeech(obj_id self, obj_id speaker, String text)
    {
        if (text.equals("test"))
        {
            dictionary dict = new dictionary();
            if (!hasObjVar(self, "myTest"))
            {
                messageTo(self, "TestCallback", dict, 1, true);
                setObjVar(self, "myTest", 1);
            }
        }
        if (text.equals("end"))
        {
            removeObjVar(self, "myTest");
        }
        return SCRIPT_CONTINUE;
    }
    public int TestCallback(obj_id self, dictionary params)
    {
        if (hasObjVar(self, "myTest"))
        {
            debugSpeakMsg(self, "bleh");
            List<Integer> test = new ArrayList<>();
            for (int i = 0; i < 2000; i++)
            {
                test.add(new Integer(i));
            }
            messageTo(self, "TestCallback", params, 1, true);
        }
        return SCRIPT_CONTINUE;
    }
}
