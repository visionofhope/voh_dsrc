package script.working.tyrone;

import script.*;
import script.base_class.*;
import script.base_script;

import script.library.*;
/*
just wrote down notes for each function. Needs more work.
TODO
    add more notes about functionality
*/

public class terminal_test extends script.base_script {
    public static final string_id SID_TT_TERMINAL = new string_id("content_tyrone", "tyrone_terminal_test");
    public static final String VOLUME_NAME = "event_volume_area";
    public int OnHearSpeech(obj_id self, obj_id speaker, String text) {
        /*
            if hasObjVar(eventVar) then
                if text = help then
                    help->helpTextWithCommands
                    help->IndexAllBuffTypes + Cost
                elseif text = buff then
                    buff->sendBuffsToPlayer(self)
            if isGod(self) then
                text = on
                    createTriggerVolume(VOLUME_NAME, 15.0f, true);
                text = off
                    removeTriggerVolume("trigTest");       
        */
        return SCRIPT_CONTINUE;
    }
    public int OnTriggerVolumeEntered(obj_id self, String volumeName, obj_id breacher) {
        if (!isMob(breacher)) {
            return SCRIPT_CONTINUE;
        }
        sendSystemMessageTestingOnly(breacher, "Welcome to the event area.");
        return SCRIPT_CONTINUE;
    }
    public int OnObjectMenuRequest(obj_id self, obj_id player, menu_info mi) {
        if (isGod(self)) {
            mi.addRootMenu(menu_info_types.ITEM_USE, SID_TT_TERMINAL);
            return SCRIPT_CONTINUE;
        }
        return SCRIPT_CONTINUE;
    }
    public int OnObjectMenuSelect(obj_id self, obj_id player, int item) {
        obj_id pInv = utils.getInventoryContainer(player);
        if (isGod(self)) {
            if (item == menu_info_types.ITEM_USE) {
                sendSystemMessageTestingOnly(self, "I'm activated");
                createTriggerVolume(VOLUME_NAME, 100.0f, true);
            }
        }
        return SCRIPT_CONTINUE;
    }
    public int OnTriggerVolumeExited(obj_id self, String triggerVolumeName, obj_id breacher) {
        //removeBuffsFromPlayer(self);
        return SCRIPT_OVERRIDE;
    }
    public int sendBuffsToPlayer() {
    /*
        if hasObjVar(eventVolumeObjVar) && isInTriggerVolume then
            givePlayerBuff + Index
            setObjVar + BuffIndex
    */
        return SCRIPT_CONTINUE;
    }
    public int removeBuffsFromPlayer() {
    /*
        if hasObjVar1 then
            removeBuff1
        if hasObjVar2 then
            removeBuff2
    */
        return SCRIPT_CONTINUE;
    }
}