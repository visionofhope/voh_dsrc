package script.conversation;

import script.library.ai_lib;
import script.library.chat;
import script.library.groundquests;
import script.library.utils;
import script.*;

public class aurilia_sarguillo extends script.conversation.base.conversation_base {
    private static final String SCRIPT = "conversation/aurilia_sarguillo";

    private boolean aurilia_sarguillo_condition_axkva_min_intro_01(obj_id player) {
        return groundquests.isTaskActive(player, "axkva_min_intro", "axkva_min_intro_01");
    }

    public int OnStartNpcConversation(obj_id self, obj_id player) {
        if (aurilia_sarguillo_condition_axkva_min_intro_01(player)) {
            OnStartNpcConversation(SCRIPT, "s_4", "s_5", player, self);
        } else {
            chat.chat(self, player, new string_id(SCRIPT, "s_4"));
        }
        return SCRIPT_CONTINUE;
    }
    public void OnNpcConversationResponse(obj_id self, String conversationName, obj_id player, string_id response) {
        switch (response.getAsciiId()) {
            case "s_5":
                npcEndConversationWithMessage(self, new string_id(SCRIPT, "s_6"));
                break;
        }
    }
}
