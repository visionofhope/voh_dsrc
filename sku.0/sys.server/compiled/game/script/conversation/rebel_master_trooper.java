package script.conversation;

import script.*;

import script.library.ai_lib;
import script.library.chat;
import script.library.space_quest;
import script.library.utils;

public class rebel_master_trooper extends script.base_script
{
    public static String c_stringFile = "conversation/rebel_master_trooper";
    public boolean rebel_master_trooper_condition_remembersPlayer(obj_id player, obj_id npc)
    {
        return (utils.hasScriptVar(player, "metNewbiePilot"));
    }
    public boolean rebel_master_trooper_condition_isImperialPilot(obj_id player, obj_id npc)
    {
        return (hasSkill(player, "pilot_imperial_navy_novice"));
    }
    public boolean rebel_master_trooper_condition_isRebelPilot(obj_id player, obj_id npc)
    {
        return (hasSkill(player, "pilot_rebel_navy_novice"));
    }
    public boolean rebel_master_trooper_condition_isPrivateerPilot(obj_id player, obj_id npc)
    {
        return hasSkill(player, "pilot_neutral_novice");
    }
    public boolean rebel_master_trooper_condition_hasSpaceExpansion(obj_id player, obj_id npc)
    {
        return true;
    }
    public boolean rebel_master_trooper_condition_hasSpaceShip(obj_id player, obj_id npc)
    {
        return (space_quest.hasShip(player));
    }
    public void rebel_master_trooper_action_rememberPlayer(obj_id player, obj_id npc)
    {
        utils.setScriptVar(player, "metNewbiePilot", true);
    }
    public void rebel_master_trooper_action_grantQuestOne(obj_id player, obj_id npc)
    {
        space_quest.grantQuest(player, "delivery", "tatooine_newbie_1");
        space_quest.grantNewbieShip(player, "rebel");
    }
    public int rebel_master_trooper_handleBranch3(obj_id player, obj_id npc, string_id response)
    {
        if (response.equals("s_33788e9b"))
        {
            doAnimationAction(npc, "nod_head_once");
            string_id message = new string_id(c_stringFile, "s_4cf446c4");
            removeObjVar(player, "conversation.rebel_master_trooper.branchId");
            npcSpeak(player, message);
            npcEndConversation(player);
            return SCRIPT_CONTINUE;
        }
        if (response.equals("s_86ed2e98"))
        {
            doAnimationAction(npc, "wave_on_dismissing");
            string_id message = new string_id(c_stringFile, "s_a366a1ca");
            removeObjVar(player, "conversation.rebel_master_trooper.branchId");
            npcSpeak(player, message);
            npcEndConversation(player);
            return SCRIPT_CONTINUE;
        }
        return SCRIPT_DEFAULT;
    }
    public int OnInitialize(obj_id self)
    {
        if ((!isMob(self)) || (isPlayer(self)))
        {
            detachScript(self, "conversation.rebel_master_trooper");
        }
        setCondition(self, CONDITION_CONVERSABLE);
        setInvulnerable(self, true);
        setCondition(self, CONDITION_SPACE_INTERESTING);
        return SCRIPT_CONTINUE;
    }
    public int OnAttach(obj_id self)
    {
        setCondition(self, CONDITION_CONVERSABLE);
        setInvulnerable(self, true);
        setCondition(self, CONDITION_SPACE_INTERESTING);
        return SCRIPT_CONTINUE;
    }
    public int OnObjectMenuRequest(obj_id self, obj_id player, menu_info menuInfo)
    {
        int menu = menuInfo.addRootMenu(menu_info_types.CONVERSE_START, null);
        menu_info_data menuInfoData = menuInfo.getMenuItemById(menu);
        menuInfoData.setServerNotify(false);
        faceTo(self, player);
        return SCRIPT_CONTINUE;
    }
    public int OnIncapacitated(obj_id self, obj_id killer)
    {
        clearCondition(self, CONDITION_CONVERSABLE);
        detachScript(self, "conversation.rebel_master_trooper");
        return SCRIPT_CONTINUE;
    }
    public boolean npcStartConversation(obj_id player, obj_id npc, String convoName, string_id greetingId, prose_package greetingProse, string_id[] responses)
    {
        Object[] objects = new Object[responses.length];
        System.arraycopy(responses, 0, objects, 0, responses.length);
        return npcStartConversation(player, npc, convoName, greetingId, greetingProse, objects);
    }
    public int OnStartNpcConversation(obj_id self, obj_id player)
    {
        obj_id npc = self;
        if (ai_lib.isInCombat(npc) || ai_lib.isInCombat(player))
        {
            return SCRIPT_OVERRIDE;
        }
        if (!rebel_master_trooper_condition_hasSpaceExpansion(player, npc))
        {
            doAnimationAction(npc, "shake_head_disgust");
            doAnimationAction(player, "shrug_hands");
            string_id message = new string_id(c_stringFile, "s_28c5a74a");
            chat.chat(npc, player, message);
            return SCRIPT_CONTINUE;
        }
        if (rebel_master_trooper_condition_isImperialPilot(player, npc))
        {
            string_id message = new string_id(c_stringFile, "s_55616900");
            chat.chat(npc, player, message);
            return SCRIPT_CONTINUE;
        }
        if (rebel_master_trooper_condition_isRebelPilot(player, npc))
        {
            string_id message = new string_id(c_stringFile, "s_7e51c66");
            string_id responses[] = new string_id[2];
            responses[0] = new string_id(c_stringFile, "s_33788e9b");
            responses[1] = new string_id(c_stringFile, "s_86ed2e98");
            setObjVar(player, "conversation.rebel_master_trooper.branchId", 3);
            npcStartConversation(player, npc, "rebel_master_trooper", message, responses);
            return SCRIPT_CONTINUE;
        }
        if (rebel_master_trooper_condition_isPrivateerPilot(player, npc))
        {
            string_id message = new string_id(c_stringFile, "s_9049d3d5");
            chat.chat(npc, player, message);
            return SCRIPT_CONTINUE;
        }
        string_id message = new string_id(c_stringFile, "s_75207c3");
        chat.chat(npc, player, message);
        return SCRIPT_CONTINUE;
    }
    public int OnNpcConversationResponse(obj_id self, String conversationId, obj_id player, string_id response)
    {
        if (!conversationId.equals("rebel_master_trooper"))
        {
            return SCRIPT_CONTINUE;
        }
        obj_id npc = self;
        int branchId = getIntObjVar(player, "conversation.rebel_master_trooper.branchId");
        if (branchId == 3 && rebel_master_trooper_handleBranch3(player, npc, response) == SCRIPT_CONTINUE)
        {
            return SCRIPT_CONTINUE;
        }
        chat.chat(npc, "Error:  Fell through all branches and responses for OnNpcConversationResponse.");
        removeObjVar(player, "conversation.rebel_master_trooper.branchId");
        return SCRIPT_CONTINUE;
    }
}
