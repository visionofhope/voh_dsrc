package script.conversation;

import script.library.ai_lib;
import script.library.chat;
import script.library.groundquests;
import script.library.utils;
import script.*;

public class aurilia_chief_engineer extends script.conversation.base.conversation_base {
    private static final String SCRIPT = "conversation/aurilia_chief_engineer";

    public void OnStartNpcConversation(obj_id self, obj_id player) {
        doAnimationAction(self, "embarrassed");
        boolean introActive = groundquests.isTaskActive(player, "axkva_min_intro", "axkva_min_intro_01");
        if (introActive) {
            OnStartNpcConversation(SCRIPT, "s_4", "s_5", player, self);
        } else {
            chat.chat(self, player, new string_id(SCRIPT, "s_4"));
        }
    }

    public void OnNpcConversationResponse(obj_id self, String conversationName, obj_id player, string_id response) {
        switch(response.getAsciiId()) {
            case "s_5":
                doAnimationAction(self, "nervous");
                npcEndConversationWithMessage(player, new string_id(SCRIPT, "s_6"));
                break;
        }
    }
}
