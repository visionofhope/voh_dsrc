package script.conversation;

import script.dictionary;
import script.library.ai_lib;
import script.library.chat;
import script.obj_id;
import script.string_id;

public class aurilia_buff_vendor_convo extends script.conversation.base.conversation_base {
    public static final String SCRIPT = "conversation/aurilia_buff_vendor_convo";

    public void OnStartNpcConversation(obj_id self, obj_id player) {
        if (!ai_lib.isInCombat(self) && !ai_lib.isInCombat(player)) {
            showTokenVendorUI(player, self);
            chat.chat(self, player, new string_id(SCRIPT, "s_4"));
        }
    }
}
