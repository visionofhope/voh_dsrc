package script.item.camp;

import script.*;

public class camp_elite extends script.item.camp.camp_base
{
    public int OnAttach(obj_id self)
    {
        setObjVar(self, "campPower", 5);
        setObjVar(self, "skillReq", 75);
        return SCRIPT_CONTINUE;
    }
}
