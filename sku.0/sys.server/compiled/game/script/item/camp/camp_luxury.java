package script.item.camp;

import script.*;

public class camp_luxury extends script.item.camp.camp_base
{
    public int OnAttach(obj_id self)
    {
        setObjVar(self, "campPower", 6);
        setObjVar(self, "skillReq", 80);
        return SCRIPT_CONTINUE;
    }
}
