package script.item;

import script.*;
import script.library.static_item;
import script.library.utils;

public class veteran_reward_crate_exp_buff extends script.base_script
{
    public static final String STF_FILE = "npe";
    public int OnObjectMenuRequest(obj_id self, obj_id player, menu_info mi)
    {
        mi.addRootMenu(menu_info_types.ITEM_USE, new string_id(STF_FILE, "crate_use"));
        return SCRIPT_CONTINUE;
    }
    public int OnObjectMenuSelect(obj_id self, obj_id player, int item)
    {
        if (item == menu_info_types.ITEM_USE)
        {
            obj_id pInv = utils.getInventoryContainer(player);
            sendSystemMessage(player, new string_id(STF_FILE, "opened_crate"));
            static_item.createNewItemFunction("item_vet_exp_buff_item_03_01", pInv);
            destroyObject(self);
        }
        return SCRIPT_CONTINUE;
    }
}
