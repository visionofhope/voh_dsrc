package script.item.dice;

import script.*;

import script.library.utils;
import script.library.xp;
import script.library.craftinglib;

public class chance_cube extends script.item.dice.base.base_dice
{
    public static final String DICE_FACE_COLOR_RED = "red";
    public static final String DICE_FACE_COLOR_BLUE = "blue";
    public static final byte DICE_FACE_COUNT = 2;
    public static final String TYPE_NAME = "chance_cube";
    public int OnInitialize(obj_id self)
    {
        setObjVar(self, DICE_TYPE_NAME, TYPE_NAME);
        return super.OnInitialize(self);
    }
    public int roll(obj_id self, dictionary params)
    {
        obj_id player = params.getObjId("player");
        xp.grantCraftingXpChance(self, player, 40);
        int rollResult = rand(DEFAULT_DICE_COUNT, DICE_FACE_COUNT, 0);
        if (rollResult == 1)
        {
            setObjVar(self, VAR_ROLL_RESULT, DICE_FACE_COLOR_RED);
        }
        if (rollResult == 2)
        {
            setObjVar(self, VAR_ROLL_RESULT, DICE_FACE_COLOR_BLUE);
        }
        return SCRIPT_CONTINUE;
    }
    public int OnDetach(obj_id self)
    {
        return super.OnDetach(self);
    }
    public int OnGetAttributes(obj_id self, obj_id player, String[] names, String[] attribs)
    {
        int idx = utils.getValidAttributeIndex(names);
        if (idx == -1)
        {
            return SCRIPT_CONTINUE;
        }
        if (hasObjVar(self, craftinglib.COMPONENT_ATTRIBUTE_OBJVAR_NAME + ".useModifier"))
        {
            names[idx] = "usemodifier";
            int attrib = (int)getFloatObjVar(self, craftinglib.COMPONENT_ATTRIBUTE_OBJVAR_NAME + ".useModifier");
            attribs[idx] = "" + attrib;
            idx++;
            if (idx >= names.length)
            {
                return SCRIPT_CONTINUE;
            }
        }
        return SCRIPT_CONTINUE;
    }
}
