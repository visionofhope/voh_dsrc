package script.item.special;

import script.*;

import script.library.utils;

public class cannot_place_in_space extends script.base_script
{
    public cannot_place_in_space()
    {
    }
    public int OnAboutToBeTransferred(obj_id self, obj_id destContainer, obj_id transferer)
    {
        location here = getLocation(self);
        String zone = here.getArea();
        if (zone.startsWith("space"))
        {
            if (!utils.isNestedWithinAPlayer(destContainer))
            {
                sendSystemMessage(transferer, new string_id("spam", "cannot_place_in_space"));
                return SCRIPT_OVERRIDE;
            }
        }
        return SCRIPT_CONTINUE;
    }
}
