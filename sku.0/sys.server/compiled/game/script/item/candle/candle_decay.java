package script.item.candle;

import script.*;

public class candle_decay extends script.base_script
{
    public static final int DECAY_LOOP_TIME = 30 * 60;
    public int OnInitialize(obj_id self)
    {
        removeObjVar(self, "timeStamp");
        removeObjVar(self, "decayTime");
        String templateName = getTemplateName(self);
        if (isLit(self) || templateName.contains("candlestick"))
        {
            return SCRIPT_CONTINUE;
        }
        templateName = getTemplateName(self);
        int idx = templateName.indexOf("frn_all_");
        if (idx != -1)
        {
            String newTemplateName = templateName.substring(0, idx) + "frn_all_light_" + templateName.substring(idx + 8);
            obj_id newLight;
            if (isInHouse(self))
            {
                newLight = createObjectAt(newTemplateName, self);
                if (isIdValid(newLight))
                {
                    setYaw(newLight, getYaw(self));
                }
            }
            else 
            {
                obj_id container = getContainedBy(self);
                if (isIdValid(container))
                {
                    newLight = createObject(newTemplateName, container, "");
                }
                else 
                {
                    newLight = createObjectAt(newTemplateName, self);
                    if (isIdValid(newLight))
                    {
                        setYaw(newLight, getYaw(self));
                    }
                }
            }
            if (!isIdValid(newLight) || newLight == self)
            {
                detachScript(self, "item.candle.candle_decay");
                return SCRIPT_CONTINUE;
            }
            else 
            {
                attachScript(newLight, "item.candle.candle_decay");
                destroyObject(self);
            }
        }
        return SCRIPT_CONTINUE;
    }
    public boolean isLit(obj_id light)
    {
        String templateName = getTemplateName(light);
        int idx = templateName.indexOf("frn_all_light_");
        return (idx != -1);
    }
    public boolean isInHouse(obj_id light)
    {
        obj_id container = getContainedBy(light);
        if (!isIdValid(container))
        {
            return false;
        }
        int got = getGameObjectType(container);
        return !(isGameObjectTypeOf(got, GOT_misc_container) || isGameObjectTypeOf(got, GOT_misc_container_wearable) || isGameObjectTypeOf(got, GOT_tool) || isGameObjectTypeOf(got, GOT_installation));
    }
}
