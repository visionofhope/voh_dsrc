package script.npe;

import script.*;

public class npe_notify_journal extends script.base_script
{
    public int OnReceivedItem(obj_id self, obj_id srcContainer, obj_id transferer, obj_id item)
    {
        if (isPlayer(item))
        {
            attachScript(item, "npe.trigger_journal");
        }
        return SCRIPT_CONTINUE;
    }
}
