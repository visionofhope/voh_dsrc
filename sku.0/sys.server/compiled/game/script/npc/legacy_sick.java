package script.npc;

import script.*;

import script.library.ai_lib;
import script.library.chat;
import script.library.utils;

public class legacy_sick extends script.base_script {
    public static final String PP_FILE_LOC = "quest/legacy/legacy_sick";
    public static final String RESPONSE_TEXT = "datatables/npc/legacy/legacy_sick.iff";
    public int OnAttach(obj_id self) {
        if (hasScript(self, "ai.creature_combat")) {
            detachScript(self, "ai.creature_combat");
        }
        if (hasScript(self, "systems.combat.credit_for_kills")) {
            detachScript(self, "systems.combat.credit_for_kills");
        }
        if (hasScript(self, "systems.combat.combat_actions")) {
            detachScript(self, "systems.combat.combat_actions");
        }
        ai_lib.setDefaultCalmBehavior(self, ai_lib.BEHAVIOR_STOP);
        return SCRIPT_CONTINUE;
    }
    public int OnTriggerVolumeEntered(obj_id self, String volumeName, obj_id breacher) {
        prose_package pp = new prose_package(PP_FILE_LOC, utils.dataTableGetString(RESPONSE_TEXT, rand(0, 6), 1));
        pp.setTT(breacher);
        chat.publicChat(self, null, null, null, pp);
        ai_lib.doAction(self, "heavy_cough_vomit");
        return SCRIPT_CONTINUE;
    }
}
