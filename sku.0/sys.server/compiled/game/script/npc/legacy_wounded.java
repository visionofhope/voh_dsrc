package script.npc;

import script.*;
import script.library.chat;
import script.library.utils;

public class legacy_wounded extends script.base_script {
    public static final String PP_FILE_LOC = "quest/legacy/legacy_wounded";
    public static final String RESPONSE_TEXT = "datatables/npc/legacy/legacy_lamentations.iff";
    public int OnAttach(obj_id self) {
        if (hasScript(self, "ai.ai")) {
            detachScript(self, "ai.ai");
        }
        if (hasScript(self, "ai.creature_combat")) {
            detachScript(self, "ai.creature_combat");
        }
        if (hasScript(self, "systems.combat.credit_for_kills")) {
            detachScript(self, "systems.combat.credit_for_kills");
        }
        if (hasScript(self, "systems.combat.combat_actions")) {
            detachScript(self, "systems.combat.combat_actions");
        }
        setPosture(self, POSTURE_KNOCKED_DOWN);
        return SCRIPT_CONTINUE;
    }
    public int OnTriggerVolumeEntered(obj_id self, String volumeName, obj_id breacher) {
        prose_package pp = new prose_package(PP_FILE_LOC, utils.dataTableGetString(RESPONSE_TEXT, rand(0, 9), 1));
        pp.setTT(breacher);
        chat.publicChat(self, null, null, null, pp);
        return SCRIPT_CONTINUE;
    }
}
