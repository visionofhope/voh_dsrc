package script.npc;

import script.*;

import script.library.stealth;

public class prof_spy_21_steal extends script.base_script
{
    public int OnAttach(obj_id self)
    {
        stealth.addStealableTemplate(self, "object/tangible/quest/prof_spy_21_incom_plans.iff");
        return SCRIPT_CONTINUE;
    }
}
