package script.library;

import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;
import script.base_script;

import script.library.dot;
import script.library.chat;
import script.library.utils;
import script.library.consumable;
import script.library.trial;

public class meditation extends script.base_script
{
    public static final float TIME_TICK = 5.0f;
    public static final float DELAY_MIN = 3f;
    public static final float DELAY_MAX = 7f;
    public static final float RAND_MODIFIER = 0.33f;
    public static final float POWERBOOST_RAMP = 60f;
    public static final String MOD_MEDITATE = "meditate";
    public static final String VAR_MEDITATION_BASE = "meditation";
    public static final String VAR_POWERBOOST_ACTIVE = "meditation.powerBoost";
    public static final String VAR_FORCE_OF_WILL_ACTIVE = "meditation.forceOfWill";
    public static final String SCRIPT_MEDITATE = "player.skill.meditate";
    public static final String HANDLER_MEDITATION_TICK = "handleMeditationTick";
    public static final String HANDLER_POWERBOOST_WANE = "handlePowerBoostWane";
    public static final String HANDLER_POWERBOOST_END = "handlePowerBoostEnd";
    public static final String MEDITATE_BUFF = "fs_meditate_";
    public static int getMeditationSkillMod(obj_id player)
    {
        if (!isIdValid(player))
        {
            return -1;
        }
        int meditate = getEnhancedSkillStatisticModifierUncapped(player, MOD_MEDITATE);
        return meditate;
    }
    public static boolean startMeditation(obj_id player)
    {
        if (!isIdValid(player))
        {
            return false;
        }
        setState(player, STATE_MEDITATE, true);
        chat.setTempAnimationMood(player, "meditating");
        messageTo(player, HANDLER_MEDITATION_TICK, trial.getSessionDict(player, meditation.HANDLER_MEDITATION_TICK), TIME_TICK, false);
        return true;
    }
    public static void endMeditation(obj_id player, boolean verbose)
    {
        if (!isIdValid(player))
        {
            return;
        }
        setState(player, STATE_MEDITATE, false);
        chat.resetTempAnimationMood(player);
        utils.removeScriptVar(player, VAR_MEDITATION_BASE);
        trial.bumpSession(player, meditation.HANDLER_MEDITATION_TICK);
    }
    public static void endMeditation(obj_id player)
    {
        endMeditation(player, true);
    }
    public static boolean isMeditating(obj_id player)
    {
        return isIdValid(player) && getState(player, STATE_MEDITATE) == 1;
    }
    public static float trance(obj_id player)
    {
        if (!isIdValid(player))
        {
            return -1f;
        }
        int modval = getMeditationSkillMod(player);
        if ((modval < 1) || !isMeditating(player))
        {
            return -1f;
        }
        float delay = 0f;
        LOG("meditate", "trance: slotDot -> DOT_BLEEDING");
        delay = slowDOT(player, modval, dot.DOT_BLEEDING);
        if (modval > 20 && delay == 0f)
        {
            LOG("meditate", "trance: slotDot -> DOT_POISON");
            delay = slowDOT(player, modval, dot.DOT_POISON);
        }
        if (modval > 40 && delay == 0f)
        {
            LOG("meditate", "trance: slotDot -> DOT_DISEASE");
            delay = slowDOT(player, modval, dot.DOT_DISEASE);
        }
        if (modval > 60 && delay == 0f)
        {
            LOG("meditate", "trance: cureWounds...");
            delay = cureWounds(player, modval);
        }
        LOG("meditate", "trance: pre-ret delay = " + delay);
        if (delay > 0f)
        {
            if (delay < DELAY_MIN)
            {
                delay = DELAY_MIN;
            }
            if (delay > DELAY_MAX)
            {
                delay = DELAY_MAX;
            }
        }
        return delay;
    }
    public static float slowDOT(obj_id player, int modval, String dotType)
    {
        if (!isIdValid(player))
        {
            return 0f;
        }
        int totalCured = 0;
        int toCure = getDOTReductionAmount(player);
        if (toCure > 0)
        {
            totalCured = dot.reduceDotTypeStrength(player, dotType, toCure);
        }
        if (totalCured < 0f)
        {
            return 0f;
        }
        return 5f;
    }
    public static int getDOTReductionAmount(obj_id player)
    {
        if (!isIdValid(player))
        {
            return -1;
        }
        int dotMod = getSkillStatMod(player, "private_med_dot");
        float ret = (float)dotMod * (1f + rand(-RAND_MODIFIER, RAND_MODIFIER));
        return Math.round(ret);
    }
    public static int getWoundReductionAmount(obj_id player)
    {
        if (!isIdValid(player))
        {
            return -1;
        }
        int dotMod = getSkillStatMod(player, "private_med_wound");
        float ret = (float)dotMod * (1f + rand(-RAND_MODIFIER, RAND_MODIFIER));
        return Math.round(ret);
    }
    public static float cureWounds(obj_id player, int modval)
    {
        if (!isIdValid(player))
        {
            return 0f;
        }
        dictionary toHeal = new dictionary();
        if ((toHeal == null) || (toHeal.isEmpty()))
        {
            LOG("meditate", "cureWounds: no wounds...");
            return 0f;
        }
        return 5f;
    }
    public static boolean powerBoost(obj_id player)
    {
        if (!isIdValid(player))
        {
            return false;
        }
        int modval = getMeditationSkillMod(player);
        if (modval < 1)
        {
            return false;
        }
        if (!isMeditating(player))
        {
            return false;
        }
        if (hasObjVar(player, VAR_POWERBOOST_ACTIVE))
        {
            return false;
        }
        float duration = 300f + (float)(modval) * 3 * rand(0.95f, 1.05f);
        int boost = 500;
        addAttribModifier(player, "meditation.powerboost.health", HEALTH, boost, duration, POWERBOOST_RAMP, POWERBOOST_RAMP, false, false, false);
        addAttribModifier(player, "meditation.powerboost.action", ACTION, boost, duration, POWERBOOST_RAMP, POWERBOOST_RAMP, false, false, false);
        int wane_time = (int)(duration + POWERBOOST_RAMP);
        int expire_time = wane_time + (int)POWERBOOST_RAMP;
        int expiration = getGameTime() + expire_time;
        setObjVar(player, VAR_POWERBOOST_ACTIVE, expiration);
        dictionary d = new dictionary();
        d.put("expiration", expiration);
        messageTo(player, HANDLER_POWERBOOST_WANE, d, wane_time, false);
        messageTo(player, HANDLER_POWERBOOST_END, d, expire_time, true);
        messageTo(player, "handlePowerBoostLog", null, POWERBOOST_RAMP * 2, false);
        return true;
    }
}
