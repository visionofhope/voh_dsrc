package script.library;

public class attrib extends script.base_script {
    public static final byte ALL = 1;
    public static final byte HERBIVORE = 2;
    public static final byte CARNIVORE = 3;
    public static final byte MONSTER = 4;
    public static final byte NPC = 5;
    public static final byte REBEL = 6;
    public static final byte IMPERIAL = 7;
    public static final byte HUTT = 8;
    public static final byte TOWNSPERSON = 9;
    public static final byte TOWNPERSON = 9;
    public static final byte TUSKEN_RAIDER = 10;
    public static final byte GUARD = 11;
    public static final byte THUG = 12;
    public static final byte VICTIM = 13;
    public static final byte VEHICLE = 14;
    public static final byte SPACE_SHIP = 15;
    public static final byte BEAST = 16;
    public static final byte OUTBREAK_SURVIVOR = 17;
    public static final byte OUTBREAK_AFFLICTED = 18;
    public static final byte OUTBREAK_SURVIVOR_GUARD = 19;
}
