package script.systems.battlefield;

import script.*;

import java.util.ArrayList;
import java.util.List;

import script.library.battlefield;
import script.library.regions;
import script.library.pclib;
import script.library.factions;
import script.library.player_structure;
import script.library.sui;
import script.library.utils;

public class player_battlefield extends script.base_script
{
    public static final String VAR_PLACED_STRUCTURE = "battlefield.placed_structure";
    public int OnInitialize(obj_id self)
    {
        return SCRIPT_CONTINUE;
    }
    public int OnLogout(obj_id self)
    {
        region bf = battlefield.getBattlefield(self);
        if (bf == null)
        {
            return SCRIPT_CONTINUE;
        }
        obj_id master_object = battlefield.getMasterObjectFromRegion(bf);
        battlefield.expelPlayerFromBattlefield(self, master_object);
        return SCRIPT_CONTINUE;
    }
    public int OnDetach(obj_id self)
    {
        if (hasObjVar(self, battlefield.VAR_BATTLEFIELD))
        {
            removeObjVar(self, battlefield.VAR_BATTLEFIELD);
        }
        if (utils.hasScriptVar(self, battlefield.VAR_SELECTING_FACTION))
        {
            utils.removeScriptVar(self, battlefield.VAR_SELECTING_FACTION);
        }
        return SCRIPT_CONTINUE;
    }
    public int OnPlaceStructure(obj_id self, obj_id player, obj_id deed, location position, int rotation)
    {
        if (player != deed)
        {
            return SCRIPT_CONTINUE;
        }
        if (!hasObjVar(player, VAR_PLACED_STRUCTURE))
        {
            return SCRIPT_CONTINUE;
        }
        region bf = battlefield.getBattlefield(self);
        obj_id master_object = battlefield.getMasterObjectFromRegion(bf);
        int faction_id = pvpBattlefieldGetFaction(player, bf);
        String faction = factions.getFactionNameByHashCode(faction_id);
        if (!battlefield.isNearBattlefieldConstructor(master_object, position, faction))
        {
            sendSystemMessageTestingOnly(self, "This is too far away from one of your constructors.");
            return SCRIPT_OVERRIDE;
        }
        String template = getStringObjVar(player, VAR_PLACED_STRUCTURE);
        removeObjVar(player, VAR_PLACED_STRUCTURE);
        dictionary stats = battlefield.getBuildableStructureStats(master_object, template);
        if (stats == null)
        {
            sendSystemMessageTestingOnly(player, "You cannot build that in this battlefield.");
            return SCRIPT_OVERRIDE;
        }
        region bf_check = battlefield.getBattlefield(position);
        if (bf_check == null)
        {
            sendSystemMessageTestingOnly(player, "You must place structures within the battlefield.");
            return SCRIPT_CONTINUE;
        }
        int cost = stats.getInt("cost");
        int build_points = battlefield.getFactionBuildPoints(master_object, faction);
        if (build_points < cost)
        {
            sendSystemMessageTestingOnly(self, "There are insufficent build points remaining.");
            return SCRIPT_OVERRIDE;
        }
        battlefield.decrementFactionBuildPoints(master_object, faction, cost);
        float placement_height = canPlaceStructure(template, position, rotation);
        if (placement_height == -9997.0f)
        {
            sendSystemMessageTestingOnly(player, "Internal code error: canPlaceStructure");
            return SCRIPT_CONTINUE;
        }
        if (placement_height == -9998.0f)
        {
            sendSystemMessageTestingOnly(player, "Internal script error: OnPlaceStructure");
            return SCRIPT_CONTINUE;
        }
        if (placement_height == -9999.0f)
        {
            sendSystemMessageTestingOnly(player, "There is no room to place the structure here.");
            return SCRIPT_CONTINUE;
        }
        position.setY(getHeightAtLocation(position.getX(), position.getZ()));
        stats.put("height", placement_height);
        battlefield.startBuildingConstruction(master_object, player, position, rotation, stats);
        return SCRIPT_OVERRIDE;
    }
    public int msgBattlefieldFactionSelected(obj_id self, dictionary params)
    {
        utils.removeScriptVar(self, battlefield.VAR_SELECTING_FACTION);
        if (hasObjVar(self, battlefield.VAR_BATTLEFIELD_TO_ENTER))
        {
            String button = params.getString("buttonPressed");
            if (button.equals("Cancel") || button.equals("No"))
            {
                removeObjVar(self, battlefield.VAR_BATTLEFIELD);
                return SCRIPT_CONTINUE;
            }
            obj_id master_object = getObjIdObjVar(self, battlefield.VAR_BATTLEFIELD_TO_ENTER);
            removeObjVar(self, battlefield.VAR_BATTLEFIELD);
            if (master_object.isLoaded())
            {
                int selected_faction_id = pvpGetAlignedFaction(self);
                String selected_faction = factions.getFactionNameByHashCode(selected_faction_id);
                if (selected_faction == null)
                {
                    String[] factions = battlefield.getFactionsAllowed(master_object);
                    int row_selected = sui.getListboxSelectedRow(params);
                    if (row_selected == -1)
                    {
                        return SCRIPT_CONTINUE;
                    }
                    if (row_selected >= factions.length)
                    {
                        sendSystemMessageTestingOnly(self, "That is an invalid selection.");
                        return SCRIPT_CONTINUE;
                    }
                    selected_faction = factions[row_selected];
                }
                if (battlefield.isFactionRemaining(master_object, selected_faction))
                {
                    if (battlefield.canJoinFaction(master_object, self, selected_faction))
                    {
                        battlefield.addPlayerToTeam(self, selected_faction, master_object);
                    }
                }
                else 
                {
                    sendSystemMessageTestingOnly(self, "That faction has been eliminated from the battle.");
                }
            }
        }
        return SCRIPT_CONTINUE;
    }
    public int msgEndBattlefieldGame(obj_id self, dictionary params)
    {
        if (hasObjVar(self, battlefield.VAR_BATTLEFIELD))
        {
            region bf = battlefield.getBattlefield(self);
            if (bf != null)
            {
                obj_id bf_current = battlefield.getMasterObjectFromRegion(bf);
                obj_id bf_entered = battlefield.getBattlefieldEntered(self);
                if (bf_current == bf_entered)
                {
                    String faction = battlefield.getPlayerTeamFaction(self);
                    if (faction != null)
                    {
                        String faction_chat_room = battlefield.getChatRoomNameFaction(bf_entered, faction);
                        chatExitRoom(faction_chat_room);
                    }
                    if (hasObjVar(self, battlefield.VAR_WAYPOINTS))
                    {
                        obj_id[] waypoint_list = getObjIdArrayObjVar(self, battlefield.VAR_WAYPOINTS);
                        if (waypoint_list != null)
                        {
                            for (int i = 0; i < waypoint_list.length; i++)
                            {
                                if (isIdValid(waypoint_list[i]))
                                {
                                    destroyWaypointInDatapad(waypoint_list[i], self);
                                }
                            }
                        }
                    }
                    removeObjVar(self, battlefield.VAR_BATTLEFIELD);
                }
            }
            else 
            {
                String chatroom = params.getString("chatroom");
                chatExitRoom(chatroom);
                if (params.containsKey("chatroom_faction"))
                {
                    String chatroom_faction = params.getString("chatroom_faction");
                    chatExitRoom(chatroom_faction);
                }
                cleanPlayer(self);
            }
        }
        return SCRIPT_CONTINUE;
    }
    public int msgJoinBattlefieldChat(obj_id self, dictionary params)
    {
        String chat_room = params.getString("chat_room");
        chatEnterRoom(chat_room);
        return SCRIPT_CONTINUE;
    }
    public int msgExitBattlefieldChat(obj_id self, dictionary params)
    {
        String chat_room = params.getString("chat_room");
        chatExitRoom(chat_room);
        return SCRIPT_CONTINUE;
    }
    public int msgBattlefieldDeath(obj_id self, dictionary params)
    {
        obj_id master_object = params.getObjId("master_object");
        if (hasObjVar(self, pclib.VAR_BEEN_COUPDEGRACED))
        {
            removeObjVar(self, pclib.VAR_BEEN_COUPDEGRACED);
        }
        if (master_object != null)
        {
            if (battlefield.isBattlefieldActive(master_object))
            {
                battlefield.expelPlayerFromBattlefield(self, master_object);
                sendSystemMessageTestingOnly(self, "You are about to be resuscitated! You may not return to the battlefield until the current battle has ended.");
            }
            else 
            {
                sendSystemMessageTestingOnly(self, "You are about to be revived!");
            }
        }
        if (hasObjVar(self, "combat.intIncapacitationCount"))
        {
            setObjVar(self, "combat.intIncapacitationCount", 0);
        }
        pclib.resurrectPlayer(self);
        return SCRIPT_CONTINUE;
    }
    public int msgRemoveBattlefieldScript(obj_id self, dictionary params)
    {
        obj_id master_object = params.getObjId("master_object");
        if (master_object.isLoaded())
        {
            String chat_room = battlefield.getChatRoomNameAllFactions(master_object);
            chatExitRoom(chat_room);
            String faction = battlefield.getPlayerTeamFaction(self);
            if (faction != null)
            {
                String faction_chat_room = battlefield.getChatRoomNameFaction(master_object, faction);
                chatExitRoom(faction_chat_room);
            }
        }
        cleanPlayer(self);
        return SCRIPT_CONTINUE;
    }
    public int msgBuildStructureSelected(obj_id self, dictionary params)
    {
        String button = params.getString("buttonPressed");
        if (button.equals("Cancel"))
        {
            return SCRIPT_CONTINUE;
        }
        int row_selected = sui.getListboxSelectedRow(params);
        if (row_selected == -1)
        {
            return SCRIPT_CONTINUE;
        }
        region bf = battlefield.getBattlefield(self);
        obj_id master_object = battlefield.getMasterObjectFromRegion(bf);
        int faction_id = pvpBattlefieldGetFaction(self, bf);
        String faction = factions.getFactionNameByHashCode(faction_id);
        if (!battlefield.canBuildBattlefieldStructure(master_object, self))
        {
            return SCRIPT_CONTINUE;
        }
        String[] buildable_structures = battlefield.getBuildableStructures(master_object, faction);
        if (row_selected >= buildable_structures.length)
        {
            sendSystemMessageTestingOnly(self, "That is an invalid selected.");
            return SCRIPT_CONTINUE;
        }
        String structure_name = buildable_structures[row_selected];
        dictionary structure_stats = battlefield.getBuildableStructureStats(master_object, structure_name);
        if (structure_stats == null)
        {
            sendSystemMessageTestingOnly(self, "Unable to create that structure in this battlefield.");
            return SCRIPT_CONTINUE;
        }
        String template = structure_stats.getString("template");
        String name = structure_stats.getString("name");
        int cost = structure_stats.getInt("cost");
        int build_points = battlefield.getFactionBuildPoints(master_object, faction);
        if (build_points < cost)
        {
            sendSystemMessageTestingOnly(self, "There are insufficent build points remaining.");
            return SCRIPT_CONTINUE;
        }
        setObjVar(self, VAR_PLACED_STRUCTURE, template);
        enterClientStructurePlacementMode(self, self, template);
        return SCRIPT_CONTINUE;
    }
    public int msgGrantFactionStanding(obj_id self, dictionary params)
    {
        String faction = params.getString("faction");
        obj_id master_object = params.getObjId("battlefield");
        float standing = params.getFloat("standing");
        String player_faction = battlefield.getPlayerTeamFaction(self);
        obj_id player_battlefield = battlefield.getBattlefieldEntered(self);
        if (player_faction == null)
        {
            return SCRIPT_CONTINUE;
        }
        if (player_battlefield == null)
        {
            return SCRIPT_CONTINUE;
        }
        if (faction.equals(player_faction))
        {
            if (master_object == player_battlefield)
            {
                factions.addFactionStanding(self, faction, standing);
            }
            sendSystemMessageTestingOnly(self, "Your " + faction + " faction standing has been increased by " + standing + " for your battlefield deeds.");
        }
        return SCRIPT_CONTINUE;
    }
    public int msgAddBattlefieldWaypoint(obj_id self, dictionary params)
    {
        region bf = battlefield.getBattlefield(self);
        if (bf == null)
        {
            return SCRIPT_CONTINUE;
        }
        obj_id structure = params.getObjId("structure");
        if (structure.isLoaded())
        {
            battlefield.addBattlefieldWaypoint(self, structure);
        }
        return SCRIPT_CONTINUE;
    }
    public int msgSelectFactionPrompt(obj_id self, dictionary params)
    {
        obj_id master_object = params.getObjId("master_object");
        if (isIdValid(master_object))
        {
            String[] factions_allowed = battlefield.getFactionsAllowed(master_object);
            String prompt = "You are about to enter an active battlefield.  You must select a faction with which to ally yourself for the duration of the battle.";
            sui.listbox(self, self, prompt, sui.OK_CANCEL, "Select Faction", factions_allowed, "msgBattlefieldFactionSelected");
            utils.setScriptVar(self, battlefield.VAR_SELECTING_FACTION, 1);
        }
        return SCRIPT_CONTINUE;
    }
    public int msgVerifyFactionPrompt(obj_id self, dictionary params)
    {
        obj_id master_object = params.getObjId("master_object");
        String faction = params.getString("faction");
        if (isIdValid(master_object))
        {
            String prompt = "You are about to enter an active battlefield. Would you like to aid the " + faction + " faction?";
            sui.msgbox(self, self, prompt, sui.YES_NO, "msgBattlefieldFactionSelected");
            utils.setScriptVar(self, battlefield.VAR_SELECTING_FACTION, 1);
        }
        return SCRIPT_CONTINUE;
    }
    public int msgBattlefieldStatus(obj_id self, dictionary params)
    {
        region bf = battlefield.getBattlefield(self);
        obj_id master_object = battlefield.getMasterObjectFromRegion(bf);
        utils.removeScriptVarTree(self, "battlefield.status");
        int bp = sui.getIntButtonPressed(params);
        if (bp == sui.BP_OK)
        {
            if (!isIdValid(master_object))
            {
                sendSystemMessageTestingOnly(self, "Cannot found the battlefield. Cancelling refresh.");
            }
            else 
            {
                queueCommand(self, (-567736575), null, "", COMMAND_PRIORITY_DEFAULT);
            }
        }
        return SCRIPT_CONTINUE;
    }
    public int battlefieldStatus(obj_id self, obj_id target, String params, float defaultTime)
    {
        if (utils.hasScriptVar(self, "battlefield.status.pid"))
        {
            int oldpid = utils.getIntScriptVar(self, "battlefield.status.pid");
            sui.closeSUI(self, oldpid);
            utils.removeScriptVarTree(self, "battlefield.status");
        }
        region bf = battlefield.getBattlefield(self);
        if (bf == null)
        {
            sendSystemMessageTestingOnly(self, "You must be at a battlefield to do that.");
            return SCRIPT_CONTINUE;
        }
        obj_id master_object = battlefield.getMasterObjectFromRegion(bf);
        if (!isIdValid(master_object))
        {
            sendSystemMessageTestingOnly(self, "You must be at a battlefield to do that.");
            return SCRIPT_CONTINUE;
        }
        if (params.equals("game"))
        {
            if (!battlefield.isBattlefieldActive(bf))
            {
                sendSystemMessageTestingOnly(self, "The battlefield must be active in order to get statistics.");
            }
            else 
            {
                master_object = battlefield.getMasterObjectFromRegion(bf);
                dictionary stat_params = new dictionary();
                stat_params.put("player", self);
                messageTo(master_object, "msgGameStats", stat_params, 0.0f, true);
            }
            return SCRIPT_CONTINUE;
        }
        else if (params.equals("history"))
        {
            master_object = battlefield.getMasterObjectFromRegion(bf);
            dictionary stat_params = new dictionary();
            stat_params.put("player", self);
            messageTo(master_object, "msgGameStatHistory", stat_params, 0.0f, true);
            return SCRIPT_CONTINUE;
        }
        String name = battlefield.getBattlefieldLocalizedName(master_object);
        int pvp_type = bf.getPvPType();
        String pvp_type_str;
        if (pvp_type == regions.PVP_REGION_TYPE_BATTLEFIELD_PVP)
        {
            pvp_type_str = "Player vs Player";
        }
        else 
        {
            pvp_type_str = "Player vs Environment";
        }
        String game_type = battlefield.getBattlefieldGameType(bf);
        String status;
        int time;
        String time_starting;
        List<String> num_factions = new ArrayList<>();
        if (battlefield.isBattlefieldActive(bf))
        {
            status = "Active";
            time = battlefield.getGameTimeRemaining(bf);
            time_starting = "ending in";
            String[] factions_allowed = battlefield.getFactionsAllowed(bf);
            for (int i = 0; i < factions_allowed.length; i++)
            {
                obj_id[] faction_team = battlefield.getFactionTeam(bf, factions_allowed[i]);
                num_factions.add(factions_allowed[i] + " Team: " + faction_team.length);
            }
        }
        else 
        {
            int next_game = battlefield.getNextGameTime(bf);
            if (next_game == -9999)
            {
                status = "Disabled";
                time = -9999;
                time_starting = "";
            }
            else 
            {
                status = "Inactive";
                time = (next_game - getGameTime());
                time_starting = "starting in";
            }
        }
        String time_message;
        if (time > 0)
        {
            int[] conv_time = player_structure.convertSecondsTime(time);
            time_message = player_structure.assembleTimeRemaining(conv_time);
        }
        else 
        {
            time_message = "0 seconds.";
        }
        List<String> dsrc = new ArrayList<>();
        dsrc.add("Battlefield Name: " + name);
        dsrc.add("Battlefield Type: " + pvp_type_str);
        dsrc.add("Objective: " + game_type);
        if (time == -9999)
        {
            dsrc.add("Status: " + status);
        }
        else 
        {
            dsrc.add("Status: " + status + " (" + time_starting + " " + time_message + ")");
        }
        dsrc.addAll(num_factions);
        int pid = sui.listbox(self, self, "Battlefield Status", sui.REFRESH_CANCEL, "Battlefield Status", dsrc, "msgBattlefieldStatus");
        if (pid > -1)
        {
            utils.setScriptVar(self, "battlefield.status.pid", pid);
            utils.setScriptVar(self, "battlefield.status.target", master_object);
        }
        return SCRIPT_CONTINUE;
    }
    public int placeBattlefieldStructure(obj_id self, obj_id target, String params, float defaultTime)
    {
        region bf = battlefield.getBattlefield(self);
        if (bf == null)
        {
            sendSystemMessageTestingOnly(self, "You must be in a battlefield to do that.");
            return SCRIPT_CONTINUE;
        }
        obj_id master_object = battlefield.getMasterObjectFromRegion(bf);
        if (!battlefield.isBattlefieldActive(master_object))
        {
            sendSystemMessageTestingOnly(self, "The battlefield must be active in order to do that.");
            return SCRIPT_CONTINUE;
        }
        String faction = battlefield.getPlayerTeamFaction(self);
        if (!battlefield.canBuildBattlefieldStructure(master_object, self))
        {
            return SCRIPT_CONTINUE;
        }
        String[] buildable_structures = battlefield.getBuildableStructures(master_object, faction);
        if (buildable_structures == null)
        {
            sendSystemMessageTestingOnly(self, "You cannot build on this battlefield.");
            return SCRIPT_CONTINUE;
        }
        String[] dsrc = new String[buildable_structures.length];
        for (int i = 0; i < buildable_structures.length; i++)
        {
            dictionary structure_info = battlefield.getBuildableStructureStats(master_object, buildable_structures[i]);
            int cost = structure_info.getInt("cost");
            dsrc[i] = buildable_structures[i] + " (Cost: " + cost + ")";
        }
        int build_points = battlefield.getFactionBuildPoints(master_object, faction);
        String points = "points";
        if (build_points == 1)
        {
            points = "point";
        }
        String prompt = "Select a structure to build.\n(" + build_points + " build " + points + " remaining.)";
        sui.listbox(self, self, prompt, sui.OK_CANCEL, "Battlefield Constructor", dsrc, "msgBuildStructureSelected");
        return SCRIPT_CONTINUE;
    }
    public int purchaseReinforcement(obj_id self, obj_id target, String params, float defaultTime)
    {
        region bf = battlefield.getBattlefield(self);
        if (bf == null)
        {
            sendSystemMessageTestingOnly(self, "You must be in a battlefield to do that.");
            return SCRIPT_CONTINUE;
        }
        obj_id master_object = battlefield.getMasterObjectFromRegion(bf);
        if (target == null || target == obj_id.NULL_ID)
        {
            target = getLookAtTarget(self);
            if (target == null || target == obj_id.NULL_ID)
            {
                sendSystemMessageTestingOnly(self, "From which installation do you wish to purchase reinforcements?");
                return SCRIPT_CONTINUE;
            }
        }
        if (!battlefield.canBuildReinforcement(target))
        {
            sendSystemMessageTestingOnly(self, "Reinforcements are unavailable from that.");
            return SCRIPT_CONTINUE;
        }
        location loc_player = getLocation(self);
        location loc_target = getLocation(target);
        if (loc_player.distance(loc_target) > battlefield.REINFORCEMENT_RANGE)
        {
            sendSystemMessageTestingOnly(self, "You are too far away from the installation.");
            return SCRIPT_CONTINUE;
        }
        if (battlefield.buildReinforcement(master_object, target, self) == null)
        {
            sendSystemMessageTestingOnly(self, "Reinforcement request denied.");
        }
        else 
        {
            sendSystemMessageTestingOnly(self, "Your reinforcements have arrived.");
        }
        return SCRIPT_CONTINUE;
    }
    public int repairBattlefieldStructure(obj_id self, obj_id target, String params, float defaultTime)
    {
        region bf = battlefield.getBattlefield(self);
        if (bf == null)
        {
            sendSystemMessageTestingOnly(self, "You must be in a battlefield to do that.");
            return SCRIPT_CONTINUE;
        }
        obj_id master_object = battlefield.getMasterObjectFromRegion(bf);
        if (target == null || target == obj_id.NULL_ID)
        {
            target = getLookAtTarget(self);
            if (target == null || target == obj_id.NULL_ID)
            {
                sendSystemMessageTestingOnly(self, "Which structure do you wish to repair?");
                return SCRIPT_CONTINUE;
            }
        }
        location loc_player = getLocation(self);
        location loc_target = getLocation(target);
        if (loc_player.distance(loc_target) > battlefield.REINFORCEMENT_RANGE)
        {
            sendSystemMessageTestingOnly(self, "You are too far away from the structure.");
            return SCRIPT_CONTINUE;
        }
        battlefield.repairBattlefieldStructure(master_object, self, target);
        return SCRIPT_CONTINUE;
    }
    public void cleanPlayer(obj_id player)
    {
        if (player == null || player == obj_id.NULL_ID)
        {
            return;
        }
        if (hasObjVar(player, battlefield.VAR_WAYPOINTS))
        {
            obj_id[] waypoint_list = getObjIdArrayObjVar(player, battlefield.VAR_WAYPOINTS);
            if (waypoint_list != null)
            {
                for (int i = 0; i < waypoint_list.length; i++)
                {
                    obj_id waypoint = waypoint_list[i];
                    if (isIdValid(waypoint))
                    {
                        destroyWaypointInDatapad(waypoint, player);
                    }
                }
            }
        }
        removeObjVar(player, battlefield.VAR_BATTLEFIELD);
        detachScript(player, battlefield.SCRIPT_PLAYER_BATTLEFIELD);
    }
}
