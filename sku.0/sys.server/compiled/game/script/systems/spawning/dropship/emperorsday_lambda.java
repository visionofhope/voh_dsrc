package script.systems.spawning.dropship;

import script.dictionary;
import script.location;
import script.obj_id;

public class emperorsday_lambda extends script.systems.spawning.dropship.base
{
    public int OnAttach(obj_id self)
    {
        messageTo(self, "handleAttachDelay", null, 2f, false);
        setHibernationDelay(self, 600.0f);
        return super.OnAttach(self);
    }
    public int handleAttachDelay(obj_id self, dictionary params)
    {
        stop(self);
        setPosture(self, POSTURE_PRONE);
        messageTo(self, "takeOff", null, 600f, true);
        queueCommand(self, (-1465754503), self, "", COMMAND_PRIORITY_FRONT);
        return SCRIPT_CONTINUE;
    }
    public int changePosture(obj_id self, dictionary params)
    {
        setPosture(self, POSTURE_UPRIGHT);
        location ownLocation = getLocation(self);
        ownLocation.setZ(ownLocation.getZ() - 20.0f);
        faceTo(self, ownLocation);
        messageTo(self, "selfCleanUp", null, 60f, false);
        return SCRIPT_CONTINUE;
    }
    public int takeOff(obj_id self, dictionary params)
    {
        obj_id exitPoint = getObjIdObjVar(self, "exitpoint");
        if (isValidId(exitPoint))
        {
            faceTo(self, getLocation(exitPoint));
            messageTo(self, "moveToExitPoint", null, 2, false);
            return SCRIPT_CONTINUE;
        }
        messageTo(self, "changePosture", null, 10, false);
        return SCRIPT_CONTINUE;
    }
    public int moveToExitPoint(obj_id self, dictionary params)
    {
        obj_id exitPoint = getObjIdObjVar(self, "exitpoint");
        if (!isValidId(exitPoint))
        {
            messageTo(self, "changePosture", null, 1, false);
            return SCRIPT_CONTINUE;
        }
        setMovementPercent(self, 16);
        pathTo(self, getLocation(exitPoint));
        messageTo(self, "correctTakeOffThenCleanUp", null, 10, false);
        return SCRIPT_CONTINUE;
    }
    public int correctTakeOffThenCleanUp(obj_id self, dictionary params)
    {
        setPosture(self, POSTURE_UPRIGHT);
        messageTo(self, "selfCleanUp", null, 60f, false);
        return SCRIPT_CONTINUE;
    }
    public int selfCleanUp(obj_id self, dictionary params)
    {
        if (isIdValid(self))
        {
            destroyObject(self);
            return SCRIPT_CONTINUE;
        }
        return SCRIPT_CONTINUE;
    }
}
