package script.systems.spawning;

import script.dictionary;
import script.library.ai_lib;
import script.library.create;
import script.library.spawning;
import script.library.utils;
import script.location;
import script.obj_id;

import java.util.ArrayList;
import java.util.List;

public class spawner_patrol extends script.base_script
{
    public spawner_patrol()
    {
    }
    public static final int LOCATION_SEARCH_RADIUS = 1000;
    public int OnAttach(obj_id self)
    {
        start(self);
        return SCRIPT_CONTINUE;
    }
    public int startTheaterFromBuildout(obj_id self, dictionary params)
    {
        start(self);
        return SCRIPT_CONTINUE;
    }
    public void start(obj_id self)
    {
        messageTo(self, "setLocationArray", null, 5, false);
        if (!hasObjVar(self, "registerWithController"))
        {
            setObjVar(self, "registerWithController", 1);
        }
        dictionary dict = new dictionary();
        dict.put("spawnNum", 0);
        messageTo(self, "doInitialSpawn", dict, 10, false);
    }
    public int doInitialSpawn(obj_id self, dictionary params)
    {
        if (spawning.checkSpawnCount(self))
        {
            String strSpawnType = getStringObjVar(self, "strName");
            //int maxSpawns = getIntObjVar(self, "intSpawnCount");
            //int maxSpawns = 2;
            location[] patrolPoints = utils.getLocationArrayScriptVar(self, "patrolArray");
            if (patrolPoints == null) { // TODO: This should never be null, fix the tat_kraytcult_10 issue
                 System.out.println("patrolArray is null for strSpawnType: " + strSpawnType);
                 return SCRIPT_CONTINUE;
            }

            for (int i = 0; i < patrolPoints.length; i++)
            {
                createMob(strSpawnType, null, patrolPoints[i], 0.0f, self, false, i);
            }
        }
        return SCRIPT_CONTINUE;
    }
    public int doSpawnEvent(obj_id self, dictionary params)
    {
        if (!spawning.checkSpawnCount(self))
        {
            return SCRIPT_CONTINUE;
        }
        String strSpawn = getStringObjVar(self, "strSpawns");
        float fltRadius = getFloatObjVar(self, "fltRadius");
        location locTest = spawning.getRandomLocationInCircle(getLocation(self), fltRadius);
        if (getIntObjVar(self, "intGoodLocationSpawner") > 0)
        {
            requestLocation(self, strSpawn, locTest, rand(100, 200), getIntObjVar(self, "intSpawnCount"), true, true);
        }
        else 
        {
            createMob(strSpawn, null, locTest, fltRadius, self);
        }
        return SCRIPT_CONTINUE;
    }
    public float getClosestSize(float fltOriginalSize)
    {
        if (fltOriginalSize <= 4.0f)
        {
            return 4.0f;
        }
        if (fltOriginalSize <= 8.0f)
        {
            return 8.0f;
        }
        if (fltOriginalSize <= 12.0f)
        {
            return 12.0f;
        }
        if (fltOriginalSize <= 16.0f)
        {
            return 16.0f;
        }
        if (fltOriginalSize <= 32.0f)
        {
            return 32.0f;
        }
        else if (fltOriginalSize <= 48.0f)
        {
            return 48.0f;
        }
        else if (fltOriginalSize <= 64.0f)
        {
            return 64.0f;
        }
        else if (fltOriginalSize <= 80.0f)
        {
            return 80f;
        }
        else if (fltOriginalSize <= 96.0f)
        {
            return 96f;
        }
        return 32f;
    }
    public boolean createMob(String creature, obj_id locationObject, location spawnLoc, float spawnRadius, obj_id self)
    {
        return createMob(creature, locationObject, spawnLoc, spawnRadius, self, true, 0);
    }
    public boolean createMob(String strId, obj_id objLocationObject, location locLocation, float fltRadius, obj_id self, boolean doCallBack, int startingPoint)
    {
        if (!spawning.checkSpawnCount(self))
        {
            return false;
        }
        int intIndex = strId.indexOf(".iff");
        float fltMinSpawnTime = getFloatObjVar(self, "fltMinSpawnTime");
        float fltMaxSpawnTime = getFloatObjVar(self, "fltMaxSpawnTime");
        float fltRespawnTime = rand(fltMinSpawnTime, fltMaxSpawnTime);
        if (intIndex > -1)
        {
            obj_id objTemplate = createObject(strId, locLocation);
            if (isIdValid(objLocationObject))
            {
                destroyObject(objLocationObject);
            }
            if (!isIdValid(objTemplate))
            {
                return false;
            }
            spawning.incrementSpawnCount(self);
            utils.setScriptVar(objTemplate, "parent", self);
            utils.setScriptVar(objTemplate, "fltRespawnTime", fltRespawnTime);
            utils.setScriptVar(objTemplate, "patrolPoints", utils.getLocationArrayScriptVar(self, "patrolArray"));
            utils.setScriptVar(objTemplate, "patrolPathType", getStringObjVar(self, "patrolPathType"));
            utils.setScriptVar(objTemplate, "startingPoint", startingPoint);
            attachScript(objTemplate, "systems.spawning.patrol_spawned_tracker");
        }
        else 
        {
            if (isIdValid(objLocationObject))
            {
                destroyObject(objLocationObject);
            }
            obj_id objMob = create.object(strId, locLocation);
            if (!isIdValid(objMob))
            {
                setName(self, "BAD MOB OF TYPE " + strId);
                return false;
            }
            int intBehavior = getIntObjVar(self, "intDefaultBehavior");
            ai_lib.setDefaultCalmBehavior(objMob, intBehavior);
            spawning.incrementSpawnCount(self);
            utils.setScriptVar(objMob, "parent", self);
            utils.setScriptVar(objMob, "fltRespawnTime", fltRespawnTime);
            utils.setScriptVar(objMob, "patrolPoints", utils.getLocationArrayScriptVar(self, "patrolArray"));
            utils.setScriptVar(objMob, "patrolPathType", getStringObjVar(self, "patrolPathType"));
            utils.setScriptVar(objMob, "startingPoint", startingPoint);
            attachScript(objMob, "systems.spawning.patrol_spawned_tracker");
        }
        if (doCallBack)
        {
            if (spawning.checkSpawnCount(self))
            {
                messageTo(self, "doSpawnEvent", null, fltRespawnTime, false);
            }
        }
        return true;
    }
    public int setLocationArray(obj_id self, dictionary params)
    {
        String[] patrolPointString = utils.getStringArrayObjVar(self, "strPatrolPointNames");
        if (patrolPointString == null || patrolPointString.length == 0)
        {
            return SCRIPT_CONTINUE;
        }
        List<location> patrolPointLocation = new ArrayList<>();
        obj_id[] objects = getObjectsInRange(self, 700);
        if (objects == null || objects.length == 0)
        {
            return SCRIPT_CONTINUE;
        }
        patrolPointLocation.add(getLocation(getSelf()));
        boolean isInInstance = false;
        obj_id dungeonController = obj_id.NULL_ID;
        if (hasObjVar(self, "dungeonController"))
        {
            isInInstance = true;
        }
        if (isInInstance)
        {
            dungeonController = getObjIdObjVar(self, "dungeonController");
        }
        setName(self, getStringObjVar(self, "strName"));
        for (String aPatrolPointString : patrolPointString) {
            for (obj_id object : objects) {
                if (!isIdValid(object)) {
                    continue;
                }
                if (hasObjVar(object, "pointName")) {
                    if ((getStringObjVar(object, "pointName")).equals(aPatrolPointString)) {
                        if (isInInstance) {
                            if (getObjIdObjVar(object, "dungeonController") == dungeonController) {
                                setName(object, getStringObjVar(object, "pointName"));
                                patrolPointLocation.add(getLocation(object));
                            }
                        } else {
                            setName(object, getStringObjVar(object, "pointName"));
                            patrolPointLocation.add(getLocation(object));
                        }
                    }
                }
            }
        }
        utils.setScriptVar(self, "patrolArray", patrolPointLocation.toArray(new location[patrolPointLocation.size()]));
        return SCRIPT_CONTINUE;
    }
    public int OnLocationReceived(obj_id self, String strId, obj_id objLocationObject, location locLocation, float fltRadius)
    {
        if (isIdValid(objLocationObject))
        {
            createMob(strId, objLocationObject, locLocation, fltRadius, self);
        }
        else 
        {
            float fltMinSpawnTime = getFloatObjVar(self, "fltMinSpawnTime");
            float fltMaxSpawnTime = getFloatObjVar(self, "fltMaxSpawnTime");
            float fltRespawnTime = rand(fltMinSpawnTime, fltMaxSpawnTime);
            messageTo(self, "doSpawnEvent", null, fltRespawnTime, false);
        }
        return SCRIPT_CONTINUE;
    }
    public int spawnDestroyed(obj_id self, dictionary params)
    {
        int intCurrentSpawnCount = utils.getIntScriptVar(self, "intCurrentSpawnCount");
        intCurrentSpawnCount = intCurrentSpawnCount - 1;
        if (intCurrentSpawnCount > -1)
        {
            utils.setScriptVar(self, "intCurrentSpawnCount", intCurrentSpawnCount);
        }
        else 
        {
            utils.setScriptVar(self, "intCurrentSpawnCount", 0);
        }
        messageTo(self, "doSpawnEvent", null, 2, false);
        return SCRIPT_CONTINUE;
    }
    public int OnDestroy(obj_id self)
    {
        if (utils.hasScriptVar(self, "debugSpawnList"))
        {
            obj_id[] spawns = utils.getObjIdArrayScriptVar(self, "debugSpawnList");
            if (spawns == null || spawns.length == 0)
            {
                return SCRIPT_CONTINUE;
            }
            for (obj_id spawn : spawns) {
                if (isIdValid(spawn) && exists(spawn)) {
                    messageTo(spawn, "selfDestruct", null, 5, false);
                }
            }
        }
        return SCRIPT_CONTINUE;
    }
}
